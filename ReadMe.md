# README #
---------------------------------------------------------------------------
Step 1
---------------------------------------------------------------------------
1. Install com.companyname.studybuddy-Signed.apk 

---------------------------------------------------------------------------
Step 2 
---------------------------------------------------------------------------
Register Account 

1. Click Register button.

2. Fill in required details.

3. Click OK button.

---------------------------------------------------------------------------
Step 3
---------------------------------------------------------------------------

Login

1. Fill in username and password

2. Click login button.

---------------------------------------------------------------------------
Step 4
---------------------------------------------------------------------------

Add Subject

1. Fill in required details.

2. Click Add Subject Button. 

  A. Add Schedule 

   1. Fill in the the details required. 

   2. Click Add button.
  
   Edit Schedule 

   1. Selects the subject.

   2. Change detail/s.

   3. Click Update button.
   
   Delete Schedule

   1. Select the subject where the schedule should be deleted.

   2. Press and hold the subject's name button.

   3. Click delete.

  B. Add Grade 

   1. Selects the subject where grade should be added.

   2. Fills in the details required. 

   3. The User clicks the Add button.

     Delete Grade 

   1. Select the subject where grade should be deleted.

   2. Press and hold the grade.

   3. Click delete.

     Update Grade

   1. Select the subject and the component where grade should be updated. 

   2. Fills in the details required. 

   3. Click the update button.

     Browse Grade

   1. Select the subject where the grade is found.

  C. Add Task

   1. Select the subject where the task should be added. 

   2. Fills in the details required. 

   3. Click the add button.

     Update Task  

   1. Select the subject and the task which should be updated.

   2. Fill in the details required. 

   3. Click the update button.

      Browse Task 

   1. Select the subject where the task is found.

     Delete Task 

   1. Select the subject where the task should be deleted.

   2. Press and hold the task to be deleted.

   3. Click delete.

---------------------------------------------------------------------------
Step 5
---------------------------------------------------------------------------

File Upload 

1. Select the subject where the file should be uploaded.

2. Choose a file.

3. Select the add file button.

---------------------------------------------------------------------------
Step 6
---------------------------------------------------------------------------

Log Out

1. Click Logout