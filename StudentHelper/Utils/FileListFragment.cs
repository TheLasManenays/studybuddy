﻿using System.IO;
using Parse;
using StudentHelper;
using System.Threading.Tasks;
using Android.App;

namespace com.xamarin.recipes.filepicker
{
	using System;
	using System.Collections.Generic;
	using System.Linq;

	using Android.OS;
	using Android.Support.V4.App;
	using Android.Util;
	using Android.Views;
	using Android.Widget;

	/// <summary>
	///   A ListFragment that will show the files and subdirectories of a given directory.
	/// </summary>
	/// <remarks>
	///   <para> This was placed into a ListFragment to make this easier to share this functionality with with tablets. </para>
	///   <para> Note that this is a incomplete example. It lacks things such as the ability to go back up the directory tree, or any special handling of a file when it is selected. </para>
	/// </remarks>
	public class FileListFragment : ListFragment
	{
		public static readonly string DefaultInitialDirectory = "/";
		private FileListAdapter _adapter;
		private DirectoryInfo _directory;

		Repository _repo = new Repository();

		public override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			_adapter = new FileListAdapter(Activity, new FileSystemInfo[0]);
			ListAdapter = _adapter;
		}

		async public override void OnListItemClick(ListView l, View v, int position, long id)
		{
			var fileSystemInfo = _adapter.GetItem(position);

			if (fileSystemInfo.IsFile())
			{
				// Do something with the file.  In this case we just pop some toast.
				Log.Verbose("FileListFragment", "The file {0} was clicked.", fileSystemInfo.FullName);

				#region Added Lines
//				Java.IO.File finalFile = new Java.IO.File(fileSystemInfo.FullName);
				FileStream stream = File.OpenRead(fileSystemInfo.FullName);
				byte[] fileBytes = new byte[stream.Length];


				stream.Read(fileBytes, 0, fileBytes.Length);
				stream.Close();
				ProgressDialog progress = ProgressDialog.Show(_adapter.Context, "Please wait", "Uploading file " + fileSystemInfo.Name + "...", true);
				if(await SaveFile(fileSystemInfo.Name, fileBytes)){
					Console.WriteLine ("File Uploaded");
				}

				#endregion
				progress.Hide ();
				Activity.OnBackPressed ();
				Toast.MakeText(Activity, fileSystemInfo.FullName + " was successfuly uploaded.", ToastLength.Short).Show();
			}
			else
			{
				// Dig into this directory, and display it's contents
				RefreshFilesList(fileSystemInfo.FullName);
			}

			base.OnListItemClick(l, v, position, id);
		}

		async Task<bool> SaveFile(string fileName, byte[] fileToSave){
			await _repo.AddFile (GlobalContext.CurrentSubject, fileToSave, fileName);
			return true;
		}

		public override void OnResume()
		{
			base.OnResume();
			RefreshFilesList(DefaultInitialDirectory);
		}

		public void RefreshFilesList(string directory)
		{
			IList<FileSystemInfo> visibleThings = new List<FileSystemInfo>();
			var dir = new DirectoryInfo(directory);

			try
			{
				foreach (var item in dir.GetFileSystemInfos().Where(item => item.Exists))
				{
					visibleThings.Add(item);
				}
			}
			catch (Exception ex)
			{
				Log.Error("FileListFragment", "Couldn't access the directory " + _directory.FullName + "; " + ex);
				Toast.MakeText(Activity, "Problem retrieving contents of " + directory, ToastLength.Long).Show();
				return;
			}

			_directory = dir;

			_adapter.AddDirectoryContents(visibleThings);

			// If we don't do this, then the ListView will not update itself when then data set 
			// in the adapter changes. It will appear to the user that nothing has happened.
			ListView.RefreshDrawableState();

			Log.Verbose("FileListFragment", "Displaying the contents of directory {0}.", directory);
		}
	}
}