﻿using System;
using Parse;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;

namespace StudentHelper
{
	public class Repository
	{
		public Repository ()
		{
		}

		#region Gets
		async public Task<IEnumerable<ParseObject>> GetSubjects(){
			var masterQuery = ParseObject.GetQuery ("Subject").WhereEqualTo("createdBy",ParseUser.CurrentUser);
			return await masterQuery.FindAsync();
		}
		async public Task<IEnumerable<ParseObject>> GetCriterias(string subjectName){
			var masterQuery = ParseObject.GetQuery ("Criteria_List").WhereEqualTo("createdBy",ParseUser.CurrentUser).WhereEqualTo("subjectName",subjectName);
			return await masterQuery.FindAsync();
		}
		async public Task<IEnumerable<ParseObject>> GetSchedules(string day){
			var masterQuery = ParseObject.GetQuery ("Subject").WhereEqualTo("createdBy",ParseUser.CurrentUser).WhereEqualTo("day", day).OrderBy("timeIn");
			return await masterQuery.FindAsync ();
		}
		async public Task<IEnumerable<ParseObject>> GetSchedules(){
			var masterQuery = ParseObject.GetQuery ("Subject").WhereEqualTo("createdBy",ParseUser.CurrentUser).OrderBy("day");
			return await masterQuery.FindAsync ();
		}
		async public Task<IEnumerable<ParseObject>> GetTasks(string subjectName){
			var masterQuery = ParseObject.GetQuery ("Tasks").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo("Subject",subjectName).OrderBy ("priority");
			return await masterQuery.FindAsync ();
		}
		async public Task<IEnumerable<ParseObject>> GetTasks(){
			var masterQuery = ParseObject.GetQuery ("Tasks").WhereEqualTo ("createdBy", ParseUser.CurrentUser).OrderBy ("priority");
			return await masterQuery.FindAsync ();
		}
		async public Task<ParseObject> GetGrade(string subject){
			var masterQuery = ParseObject.GetQuery ("Subject").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo("subjectName", subject);
			return await masterQuery.FirstOrDefaultAsync ();
		}
		async public Task<IEnumerable<ParseObject>> GetFiles(string subject){
			var masterQuery = ParseObject.GetQuery("Files").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo("subjectName", subject);
			return await masterQuery.FindAsync ();
		}
		#endregion

		#region Adds
		public void AddSubject(string subjectName, string hour, string hour2, string day, string instructor, string location){
			ParseObject subject = new ParseObject ("Subject");
			subject ["createdBy"] = ParseUser.CurrentUser;
			subject ["subjectName"] = subjectName;
			subject ["timeIn"] = hour;
			subject ["timeOut"] = hour2;
			subject ["day"] = day;
			subject ["instructor"] = instructor;
			subject ["location"] = location;
			subject ["grade"] = 0;
			subject.SaveAsync ();
		}
		public void AddCriteria(string subjectName, string criteriaName, int percentage){
			ParseObject criteria = new ParseObject ("Criteria_List");
			criteria ["createdBy"] = ParseUser.CurrentUser;
			criteria ["subjectName"] = subjectName;
			criteria ["criteriaName"] = criteriaName;
			criteria ["percentage"] = percentage;
			criteria ["totalScore"] = 0;
			criteria ["overAllScore"] = 0;

			criteria.SaveAsync ();
		}
		public void AddTask(string taskName, string taskDescription, string priority, string deadline, string subjectName){
			ParseObject task = new ParseObject ("Tasks");
			task ["createdBy"] = ParseUser.CurrentUser;
			task ["taskName"] = taskName;
			task ["taskDescription"] = taskDescription;
			task ["priority"] = priority;
			task ["deadline"] = deadline;
			task ["Subject"] = subjectName;
			task ["Done"] = false;

			task.SaveAsync ();
		}
		async public Task<bool> AddFile(string subjectName, byte[] file, string fileName){
			var fileDoc = new ParseFile (fileName, file);
			await fileDoc.SaveAsync ();

			ParseObject fileObject = new ParseObject ("Files");
			fileObject ["createdBy"] = ParseUser.CurrentUser;
			fileObject ["subjectName"] = subjectName;
			fileObject ["fileDoc"] = fileDoc;
			fileObject ["fileName"] = fileName;

			await fileObject.SaveAsync ();

			return true;
		}
		#endregion

		#region Updates
		async public Task<IEnumerable<ParseObject>> UpdateCriteria(string subjectName, string criteriaName, int score, int overall){
			var criteria = await ParseObject.GetQuery ("Criteria_List").WhereEqualTo("createdBy", ParseUser.CurrentUser).WhereEqualTo("subjectName", subjectName).WhereEqualTo("criteriaName",criteriaName).FirstOrDefaultAsync();
//			criteria ["createdBy"] = ParseUser.CurrentUser;
//			criteria ["subjectName"] = subjectName;
//			criteria ["criteriaName"] = criteriaName;
//			criteria ["percentage"] = percentage;
			criteria ["totalScore"] = score;
			criteria ["overAllScore"] = overall;

//			Console.WriteLine (criteria.Get<string>("subjectName") + " " + criteria.Get<string>("criteriaName"));
			await criteria.SaveAsync ();
			var criteria2 = ParseObject.GetQuery ("Criteria_List").WhereEqualTo("createdBy", ParseUser.CurrentUser).WhereEqualTo("subjectName", subjectName);
//			Console.WriteLine (criteria2.Get<int>("totalScore"));
			return await criteria2.FindAsync();
		}
		async public void UpdateGrade(string subjectName, int grade){
			var subject = await ParseObject.GetQuery ("Subject").WhereEqualTo("createdBy", ParseUser.CurrentUser).WhereEqualTo("subjectName", subjectName).FirstOrDefaultAsync();
			subject ["grade"] = grade;

			await subject.SaveAsync ();
		}
		async public Task<IEnumerable<ParseObject>> UpdateTask(string taskName, string taskDescription, string priority, string deadline, string subjectName){
			var task = await ParseObject.GetQuery ("Tasks").WhereEqualTo("createdBy",ParseUser.CurrentUser).WhereEqualTo("taskName",taskName).FirstOrDefaultAsync();
//			task ["createdBy"] = ParseUser.CurrentUser;
//			task ["taskName"] = taskName;
			task ["taskDescription"] = taskDescription;
			task ["priority"] = priority;
			task ["deadline"] = deadline;
			task ["Subject"] = subjectName;

			await task.SaveAsync ();
			var task2 = ParseObject.GetQuery ("Tasks").WhereEqualTo("createdBy",ParseUser.CurrentUser);
			return await task2.FindAsync();
		}
		async public void UpdateTaskStatus(string taskName, string subjectName, bool isDone){
			var task = await ParseObject.GetQuery ("Tasks").WhereEqualTo("createdBy",ParseUser.CurrentUser).WhereEqualTo("taskName",taskName).WhereEqualTo("Subject", subjectName).FirstOrDefaultAsync();
			task ["Done"] = isDone;

			await task.SaveAsync ();
		}
		#endregion

		#region Deletes
		async public Task<IEnumerable<ParseObject>> DeleteTask(string taskName){
			var task = await ParseObject.GetQuery ("Tasks").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo("taskName",taskName).FirstOrDefaultAsync ();
			await task.DeleteAsync ();

			var task2 = ParseObject.GetQuery ("Tasks").WhereEqualTo ("createdBy", ParseUser.CurrentUser);
			return await task2.FindAsync ();
		}

		async public Task<IEnumerable<ParseObject>> DeleteCriteria(string taskName, string subjectName){
			var task = await ParseObject.GetQuery ("Criteria_List").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo("criteriaName",taskName).WhereEqualTo("subjectName",subjectName).FirstOrDefaultAsync ();
			await task.DeleteAsync ();

			var task2 = ParseObject.GetQuery ("Criteria_List").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo("subjectName",subjectName);
			return await task2.FindAsync ();
		}

		async public Task<IEnumerable<ParseObject>> DeleteSubject(string subjectName){
			var query3 = await ParseObject.GetQuery ("Criteria_List").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo ("subjectName", subjectName).FindAsync ();
			foreach (var items in query3) {
				await items.DeleteAsync ();
			}

			var query = await ParseObject.GetQuery ("Subject").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo ("subjectName", subjectName).FirstOrDefaultAsync ();
			await query.DeleteAsync ();

			var query2 = ParseObject.GetQuery("Subject").WhereEqualTo("createdBy",ParseUser.CurrentUser);
			return await query2.FindAsync ();
		}
		#endregion
	}
}

