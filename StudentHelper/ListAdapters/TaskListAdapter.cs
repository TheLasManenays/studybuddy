﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Net;

namespace StudentHelper
{
	public class TaskListAdapter : BaseAdapter
	{
		List<Task> _taskList = new List<Task>();
		Activity _context;
		Repository _repo = new Repository();

		public TaskListAdapter (Activity context, List<Task> taskList)
		{
			_context = context;
			_taskList = taskList;
		}
		#region implemented abstract members of BaseAdapter

		public override Java.Lang.Object GetItem (int position)
		{
			return null;
		}

		public override long GetItemId (int position)
		{
			return 0;
		}

		public override Android.Views.View GetView (int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			var view = _context.LayoutInflater.Inflate (Resource.Layout.TaskListItem, null);
			var taskName = view.FindViewById<TextView> (Resource.Id.taskName);
			var taskCheckBox = view.FindViewById<CheckBox> (Resource.Id.taskCheckBox);

			var isDone = _taskList [position].isDone;
			taskName.Text = _taskList [position].TaskName;

			if (isDone) {
				taskCheckBox.Checked = true;
			} else {
				taskCheckBox.Checked = false;
			}

			taskCheckBox.Click += (sender, e) => {

				NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
				bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

				if (isOnline) {
					bool Done = false;
					if (isDone) {
						taskCheckBox.Checked = false;
						Done = false;
						isDone = false;
					} else {
						taskCheckBox.Checked = true;
						Done = true;
						isDone = true;
					}
					_repo.UpdateTaskStatus(_taskList[position].TaskName, _taskList[position].Subject, Done);
				} else {
					Toast.MakeText(_context, "You are offline. Changes here won't take effect.", ToastLength.Short).Show();
				}
//				Toast.MakeText(_context, "Changed task status to done", ToastLength.Short).Show();
			};
			return view;
		}

		public override int Count {
			get {
				return _taskList.Count;
			}
		}

		#endregion
	}
}

