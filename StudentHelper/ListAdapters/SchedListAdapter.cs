﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;

namespace StudentHelper
{
	public class SchedListAdapter : BaseAdapter
	{
		List<Schedule> _schedList = new List<Schedule>();
		Activity _context;

		public SchedListAdapter (Activity context, List<Schedule> schedList)
		{
			this._schedList = schedList;
			this._context = context;
		}

		#region implemented abstract members of BaseAdapter

		public override Java.Lang.Object GetItem (int position)
		{
			return null;
		}

		public override long GetItemId (int position)
		{
			return 0;
		}

		public override Android.Views.View GetView (int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
		{
			var view = _context.LayoutInflater.Inflate (Resource.Layout.ScheduleTableItem, null);
			var dayText = view.FindViewById<TextView> (Resource.Id.dayText);
			var subjectText = view.FindViewById<TextView> (Resource.Id.subjectText);
			var hourText = view.FindViewById<TextView> (Resource.Id.hourText);
			var instructor = view.FindViewById<TextView> (Resource.Id.instructorText);
			var location = view.FindViewById<TextView> (Resource.Id.locationText);

//			foreach (var item in _schedList) {
			try{
			dayText.Text = _schedList[position].Day;
			subjectText.Text = _schedList[position].Subject;
			hourText.Text = _schedList[position].TimeIn + "-" + _schedList[position].TimeOut;
			location.Text = "Location: " + _schedList [position].Location;
			instructor.Text = "Instructor: " +  _schedList [position].Instructor;
//			}
			}catch(Exception e){
				Toast.MakeText (_context, "Opps! Something went wrong.", ToastLength.Short).Show ();
			}

			return view;
		}

		public override int Count {
			get {
				return _schedList.Count;
			}
		}

		#endregion
	}
}

