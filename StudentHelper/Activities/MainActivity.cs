using Android.App;
using Android.Widget;
using Android.OS;
using Android.Support.V7.App;
using V7ToolBar = Android.Support.V7.Widget.Toolbar;
using Parse;
using Android.Content;
using Android.Views.InputMethods;
using System;
using Android.Net;
using SQLite;

namespace StudentHelper
{
	[Activity (MainLauncher = true, Icon = "@mipmap/ic_launcher")]
	public class MainActivity : AppCompatActivity
	{
		string folder = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
		SQLiteConnection _conn;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.Main);

			GlobalContext._connectivityManager = (ConnectivityManager) GetSystemService(ConnectivityService);
			_conn = new SQLiteConnection (System.IO.Path.Combine (folder, "cached.db"));
			_conn.CreateTable<SQLTable_Schedule> ();
			_conn.CreateTable<SQLTable_Subject> ();
			_conn.CreateTable<SQLTable_Task> ();
			_conn.CreateTable<SQLTable_Subject_Grade> ();

			ParseClient.Initialize (GlobalContext.AppId, GlobalContext.NetKey);

			var toolBar = FindViewById<V7ToolBar> (Resource.Id.toolBar);
			SetSupportActionBar (toolBar);

			var username = FindViewById<EditText> (Resource.Id.userName);
			var pass = FindViewById<EditText> (Resource.Id.passWord);

			var loginButton = FindViewById<Button> (Resource.Id.buttonLogin);
			loginButton.Click += delegate {
				InputMethodManager inputManager = (InputMethodManager) this.GetSystemService(Context.InputMethodService);

				inputManager.HideSoftInputFromWindow(this.CurrentFocus.WindowToken, HideSoftInputFlags.NotAlways);
				Validate(username.Text, pass.Text);

				_conn.DeleteAll<SQLTable_Schedule>();
				_conn.DeleteAll<SQLTable_Subject> ();
				_conn.DeleteAll<SQLTable_Task> ();
				_conn.DeleteAll<SQLTable_Subject_Grade> ();

				_conn.CreateTable<SQLTable_Schedule> ();
				_conn.CreateTable<SQLTable_Subject> ();
				_conn.CreateTable<SQLTable_Task> ();
				_conn.CreateTable<SQLTable_Subject_Grade> ();
			};
			var signupButton = FindViewById<Button> (Resource.Id.signupButton);
			signupButton.Click += delegate {
				StartActivity(typeof(SIgnupActivity));
			};
		}

		protected override void OnResume ()
		{
			base.OnResume ();
			NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
			bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

			if (!isOnline) {
				Toast.MakeText (this, "Offline : Data will only come from stored cache.", ToastLength.Short).Show ();
				var query = _conn.Table<SQLTable_Schedule> ().Count ();
				if(query > 0)
					StartActivity (typeof(sched_activity));
			}
		}

		public override void OnBackPressed ()
		{
			Intent intent = new Intent (Intent.ActionMain);
			intent.AddCategory (Intent.CategoryHome);
			intent.SetFlags (ActivityFlags.NewTask);
			StartActivity (intent);
		}

		async private void Validate(string userName, string pass){
			try{
				await ParseUser.LogInAsync(userName,pass);

				if(ParseUser.CurrentUser != null)
					StartActivity (typeof(sched_activity));
				else
					Toast.MakeText (this, "Invalid username or password.", ToastLength.Long).Show();
			}catch(Exception ex){
				Toast.MakeText (this, "Invalid username or password.", ToastLength.Short).Show();
			}
		}
	}
}
