﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;

namespace StudentHelper
{
	[Activity (Label = "New Subject")]			
	public class SubjectFormActivity : AppCompatActivity
	{
		Repository _repo = new Repository();
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.SubjectForm);
			var toolBar = FindViewById<Toolbar> (Resource.Id.toolBar);
			SetSupportActionBar (toolBar);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);

			var subjectName = FindViewById<EditText> (Resource.Id.subjectName);
			var hourPicker = FindViewById<TimePicker> (Resource.Id.subjectTimePicker);
			var hourPicker2 = FindViewById<TimePicker> (Resource.Id.subjectTimePicker2);
			var day = FindViewById<Spinner> (Resource.Id.daySpinner);
			var location = FindViewById<EditText>(Resource.Id.locationText);
			var instructor = FindViewById<EditText>(Resource.Id.instructorText);
			var addSubject = FindViewById<Button> (Resource.Id.addSubjectButton);
			var cancel = FindViewById<Button> (Resource.Id.cancelAddSubjectButton);

			string dayString = string.Empty;
			day.ItemSelected += (sender, e) => {
				dayString = day.GetItemAtPosition (e.Position).ToString ();
			};
			var adapter = ArrayAdapter.CreateFromResource (this, Resource.Array.day_array, Android.Resource.Layout.SimpleSpinnerItem);
			adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
			day.Adapter = adapter;

			addSubject.Click += delegate {
				string am_pm = ((hourPicker.CurrentHour).IntValue () < 12) ? "AM" : "PM";
				var hour = hourPicker.CurrentHour + ":" + hourPicker.CurrentMinute + " " + am_pm;
				string am_pm2 = ((hourPicker2.CurrentHour).IntValue () < 12) ? "AM" : "PM";
				var hour2 = hourPicker2.CurrentHour + ":" + hourPicker2.CurrentMinute + " " + am_pm2;
				_repo.AddSubject (subjectName.Text, hour, hour2, dayString, instructor.Text, location.Text);
				Toast.MakeText (this, " Successfully added a subject.", ToastLength.Short).Show ();
				OnBackPressed();
			};

			cancel.Click += delegate {
				OnBackPressed();
			};
		}

		public override bool OnSupportNavigateUp ()
		{
			OnBackPressed ();
			return base.OnSupportNavigateUp ();
		}
	}
}

