﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;

namespace StudentHelper
{
	[Activity (Label = "Choose file")]			
	public class FileDisplayActivity : AppCompatActivity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.file_picker_layout_);
			var toolBar = FindViewById<Toolbar> (Resource.Id.toolBar);
			SetSupportActionBar (toolBar);
		}
	}
}

