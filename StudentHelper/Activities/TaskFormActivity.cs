﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;

namespace StudentHelper
{
	[Activity (Label = "New Task")]			
	public class TaskFormActivity : AppCompatActivity
	{
		Repository _repo = new Repository();
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			SetContentView (Resource.Layout.TaskForm);
			var toolBar = FindViewById<Toolbar> (Resource.Id.toolBar);
			SetSupportActionBar (toolBar);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);

			var fromWhere = this.Intent.GetStringExtra ("FromWhere");
			var subject = this.Intent.GetStringExtra ("Subject");

			var taskName = FindViewById<EditText>(Resource.Id.taskName);
			var taskDescription = FindViewById<EditText>(Resource.Id.taskDescription);
			var taskDatePicker = FindViewById<DatePicker>(Resource.Id.taskDatePicker);
			var taskTimePicker = FindViewById<TimePicker>(Resource.Id.taskTimePicker);
			var taskSubject = FindViewById<EditText>(Resource.Id.taskSubject);
			var addTask = FindViewById<Button> (Resource.Id.addTaskButton);
			var cancel = FindViewById<Button> (Resource.Id.cancelAddTaskButton);

			if (fromWhere != null) {
				var taskLabel = FindViewById<TextView> (Resource.Id.taskSUbjectLabel);
				taskLabel.Visibility = ViewStates.Gone;
				taskSubject.Visibility = ViewStates.Gone;
				taskSubject.Text = subject;
			}
			var priority = FindViewById<Button>(Resource.Id.prority);
			string[] priorityTypes = new string[]{"Low", "Medium", "High"};
			int index = 0;
			priority.Text = priorityTypes[index];
			priority.Click += delegate {
				if(index < 2)
					index++;
				else
					index = 0;
				priority.Text = priorityTypes[index];
			};

			addTask.Click += delegate {
				string am_pm = ((taskTimePicker.CurrentHour).IntValue() < 12) ? "AM" : "PM";
				string deadline = ("Deadline: " + (taskDatePicker.Month+1)+"/"+taskDatePicker.DayOfMonth+"/"+taskDatePicker.Year + " " + taskTimePicker.CurrentHour + ":" + taskTimePicker.CurrentMinute + " " +am_pm).ToString();
				_repo.AddTask(taskName.Text, taskDescription.Text, priority.Text, deadline, taskSubject.Text);
				Toast.MakeText (this, " Successfully added a task.", ToastLength.Short).Show ();
				OnBackPressed();
			};

			cancel.Click += delegate {
				OnBackPressed();
			};
		}

		public override bool OnSupportNavigateUp ()
		{
			OnBackPressed ();
			return base.OnSupportNavigateUp ();
		}
	}
}

