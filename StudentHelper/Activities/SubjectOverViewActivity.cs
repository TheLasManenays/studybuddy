﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using V7ToolBar = Android.Support.V7.Widget.Toolbar;
using Parse;

namespace StudentHelper
{
	[Activity (Label = "Subject Overview")]		
	public class SubjectOverViewActivity : AppCompatActivity
	{
		Repository _repo = new Repository ();
		ListView _criteriaList;
		TextView _gradeView;
		string _subject;
		List<string> _criterias = new List<string> ();
		List<string> _listItems = new List<string> ();
		IEnumerable<ParseObject> _criteriasQuery;
		int percentageTotal = 0;
		Button addCriteria;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			_subject = Intent.GetStringExtra ("Subject");
			// Create your application here
			SetContentView (Resource.Layout.SubjectOverview);
			var toolBar = FindViewById<V7ToolBar> (Resource.Id.toolBar);
			SetSupportActionBar (toolBar);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);
			this.Title = _subject;

			addCriteria = FindViewById<Button> (Resource.Id.addCriteria);
			addCriteria.Enabled = false;
			addCriteria.Click += (sender, e) => {
				if(percentageTotal >= 100){
					Toast.MakeText(this, "Criterias has already reached a total of 100 %. Delete a criteria if you want to add a new one.", ToastLength.Long).Show();
					return;
				}
				Console.WriteLine ("Add Criteria");
				LayoutInflater li = LayoutInflater.From (this);
				View ap = li.Inflate (Resource.Layout.NewCriteriaForm, null);
				var alert = new Android.Support.V7.App.AlertDialog.Builder (this);
				alert.SetView (ap);
				alert.SetTitle("New Criteria");
				var criteriaName = ap.FindViewById<EditText> (Resource.Id.criteriaName);
				var percentageVal = ap.FindViewById<EditText> (Resource.Id.percentageVal);
				alert.SetNegativeButton ("Cancel", delegate {

				});
				alert.SetPositiveButton ("Add", delegate {
					_repo.AddCriteria (_subject, criteriaName.Text, Int32.Parse (percentageVal.Text));
					Toast.MakeText (this, " Successfully added a criteria.", ToastLength.Short).Show ();
					_criterias.Clear ();
					_listItems.Clear ();
					LoadListContent ();
				});
				alert.Show ();
			};
			_gradeView = FindViewById<TextView> (Resource.Id.gradeTotalView);
			_criteriaList = FindViewById<ListView> (Resource.Id.criteriaListView);
			_criteriaList.ItemClick += (sender, e) => {
				LayoutInflater li = LayoutInflater.From (this);
				View ap = li.Inflate (Resource.Layout.UpdateScore, null);
				var alert = new Android.Support.V7.App.AlertDialog.Builder (this);
				alert.SetView (ap);
				var critScore = ap.FindViewById<EditText> (Resource.Id.critScore);
				var critOverAll = ap.FindViewById<EditText> (Resource.Id.critOverall);

				alert.SetPositiveButton ("Update", async delegate {
					_criteriasQuery = await _repo.UpdateCriteria (_subject, _criterias [e.Position], Int32.Parse (critScore.Text), Int32.Parse (critOverAll.Text));
					_criterias.Clear ();
					_listItems.Clear ();
					LoadListContent ();
					CalculateGrade ();
					Toast.MakeText (this, "Update Done.", ToastLength.Short).Show ();
				});

				alert.SetNegativeButton ("Cancel", delegate {

				});

				alert.Show ();
			};
			_criteriaList.ItemLongClick += (sender, e) => {
				var alert = new Android.Support.V7.App.AlertDialog.Builder (this);
				alert.SetTitle("Options");
				alert.SetMessage("Do you want to delete this criteria?");
				alert.SetNegativeButton("No", delegate {
					
				});
				alert.SetPositiveButton("Yes",async delegate {
					_criteriasQuery = await _repo.DeleteCriteria(_criterias[e.Position], _subject);
					_criterias.Clear ();
					_listItems.Clear ();
					CalculateGrade ();
					LoadListContent ();
					Toast.MakeText (this, "Criteria deleted.", ToastLength.Short).Show ();
				});
				alert.Show();
			};
			LoadListContent ();
		}

		public void LoadListContent ()
		{
			RunOnUiThread (async () => {
				_criteriasQuery = await _repo.GetCriterias (_subject);
				foreach (var items in _criteriasQuery) {
					try {
						_criterias.Add (items.Get<string> ("criteriaName"));
						var percentValue = items.Get<int> ("percentage");
						percentageTotal += percentValue;
//						Console.WriteLine (items.Get<string> ("criteriaName") + " : " + items.Get<int> ("percentage") + "%\t Recent score : " + items.Get<int> ("totalScore"));
						_listItems.Add ("\n" + items.Get<string> ("criteriaName") + " : " + percentValue + "%\nRecent score : " + items.Get<int> ("totalScore") + "/" + items.Get<int>("overAllScore") + "\n");
					} catch (Exception e) {
						Console.WriteLine (e.Message);
					}

					var masterQuery = ParseObject.GetQuery ("Subject").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo("subjectName", _subject);
					var gradeQuery =  await masterQuery.FirstOrDefaultAsync ();
					_gradeView.Text = "Grade Total : " + (gradeQuery.Get<int>("grade")).ToString();
				}
				var adapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, _listItems.ToArray ());
				_criteriaList.Adapter = adapter;
				addCriteria.Enabled = true;
			});
		}

		public void CalculateGrade ()
		{
			try {
				int grade = 0;
				foreach (var item in _criteriasQuery) {
					Console.WriteLine (item.Get<string> ("criteriaName") + " " + item.Get<int> ("percentage"));
					double totalScore = item.Get<int> ("totalScore");
					double overAll = item.Get<int> ("overAllScore");
					double percentage = item.Get<int> ("percentage");
					double points = (totalScore / overAll) * percentage;

					grade += Convert.ToInt32 (points);
				}
				Console.WriteLine (grade);
				_repo.UpdateGrade(_subject,grade);
			} catch (Exception e) {
				Toast.MakeText (this, e.Message, ToastLength.Long).Show ();
			}
		}

		public override bool OnSupportNavigateUp ()
		{
			OnBackPressed ();
			return base.OnSupportNavigateUp ();
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.menus, menu);
			return base.OnPrepareOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.logout:
				{
					StartActivity (typeof(MainActivity));
					ParseUser.LogOut ();
					return true;
				}
				break;
			case Resource.Id.refresh:
				{
					_criterias.Clear ();
					_listItems.Clear ();
					LoadListContent ();
					return true;
				}
				break;
			}

			return base.OnOptionsItemSelected (item);
		}
	}
}

