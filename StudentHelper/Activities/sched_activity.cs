﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;

using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V4.View;
using Android.Support.Design.Widget;

using Fragment = Android.Support.V4.App.Fragment;

using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace StudentHelper
{
	[Activity (Label = "Class Schedule")]		
	public class sched_activity : AppCompatActivity
	{
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your application here
			SetContentView(Resource.Layout.sched_layout);
			Toolbar toolBar = FindViewById<Toolbar> (Resource.Id.toolbar);
			SetSupportActionBar (toolBar);

			var viewPager = FindViewById<ViewPager> (Resource.Id.viewpager);
			if (viewPager != null) {
				setupViewPager (viewPager);
			}

			var tabLayout = FindViewById<TabLayout> (Resource.Id.tabs);
			tabLayout.SetupWithViewPager (viewPager);

			var surfaceOrientation = WindowManager.DefaultDisplay.Rotation;
			if (surfaceOrientation == SurfaceOrientation.Rotation0 || surfaceOrientation == SurfaceOrientation.Rotation180) {
				tabLayout.TabMode = TabLayout.ModeScrollable;

			} else {
				tabLayout.TabMode = TabLayout.ModeFixed;
			}

			this.Window.SetSoftInputMode (SoftInput.AdjustPan);
		}

		public override void OnBackPressed ()
		{
			Intent intent = new Intent (Intent.ActionMain);
			intent.AddCategory (Intent.CategoryHome);
			intent.SetFlags (ActivityFlags.NewTask);
			StartActivity (intent);
		}

		void setupViewPager (ViewPager viewPager)
		{
			var adapter = new Adapter (SupportFragmentManager);
			adapter.AddFragment (new sched_fragment ().getInstance (this, "All"), "All");
			adapter.AddFragment (new sched_fragment ().getInstance (this, "Sunday"), "Sunday");
			adapter.AddFragment (new sched_fragment ().getInstance (this, "Monday"), "Monday");
			adapter.AddFragment (new sched_fragment ().getInstance (this, "Tuesday"), "Tuesday");
			adapter.AddFragment (new sched_fragment ().getInstance (this, "Wednesday"), "Wednesday");
			adapter.AddFragment (new sched_fragment ().getInstance (this, "Thursday"), "Thursday");
			adapter.AddFragment (new sched_fragment ().getInstance (this, "Friday"), "Friday");
			adapter.AddFragment (new sched_fragment ().getInstance (this, "Saturday"), "Saturday");
			viewPager.Adapter = adapter;
		}

		class Adapter : Android.Support.V4.App.FragmentPagerAdapter
		{
			List<Fragment> fragments = new List<Fragment> ();
			List<string> fragmentTitles = new List<string> ();

			public Adapter (FragmentManager fm) : base (fm)
			{
			}

			public void AddFragment (Fragment fragment, String title)
			{
				fragments.Add (fragment);
				fragmentTitles.Add (title);
			}

			public override Fragment GetItem (int position)
			{
				return fragments [position];
			}

			public override int Count {
				get { return fragments.Count; }
			}

			public override Java.Lang.ICharSequence GetPageTitleFormatted (int position)
			{
				return new Java.Lang.String (fragmentTitles [position]);
			}

		}
	}
}

