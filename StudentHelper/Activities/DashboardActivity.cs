﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using V7ToolBar = Android.Support.V7.Widget.Toolbar;

using Parse;
using Android.Net;
using SQLite;

namespace StudentHelper
{
	[Activity (Label = "Grades")]			
	public class DashboardActivity : AppCompatActivity
	{
		Repository _repo = new Repository ();
		ListView _subjectList;
		IEnumerable<ParseObject> _subjectsQuery;
		List<string> _subjects = new List<string> ();
		List<string> _listItems = new List<string> ();

		bool _isOnline;

		string folder = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
		SQLiteConnection _conn;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			_conn = new SQLiteConnection (System.IO.Path.Combine (folder, "cached.db"));

			SetContentView (Resource.Layout.Dashboard);
			var toolBar = FindViewById<V7ToolBar> (Resource.Id.toolBar);
			SetSupportActionBar (toolBar);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);

			_subjectList = FindViewById<ListView> (Resource.Id.subjectList);

			_subjectList.ItemClick += (sender, e) => {
				if(_isOnline){
					Intent next = new Intent (this, typeof(SubjectOverViewActivity));
					try {
						next.PutExtra ("Subject", _subjects [e.Position]);
						StartActivity (next);
					} catch (Exception ex) {
						Console.WriteLine (ex.Message);
					}
				}
				else
					Toast.MakeText(this, "You can only view grades when offline", ToastLength.Short).Show();
			};

			_subjectList.ItemLongClick += (sender, e) => {
				if(_isOnline){
					var alert = new Android.Support.V7.App.AlertDialog.Builder (this);
					alert.SetTitle ("Options");
					alert.SetMessage ("Do you want to delete this subject?");
					alert.SetNegativeButton ("No", delegate {

					});
					alert.SetPositiveButton ("Yes", async delegate {
						_subjectsQuery = await _repo.DeleteSubject (_subjects [e.Position]);
						_subjects.Clear ();
						_listItems.Clear ();
						LoadListContent ();
					});
					alert.Show ();
					}
				else
					Toast.MakeText(this, "You can only view grades when offline", ToastLength.Short).Show();
			};
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
			_isOnline = (activeConnection != null) && activeConnection.IsConnected;

			if (_isOnline) {
				_subjects.Clear ();
				_listItems.Clear ();
				LoadListContent ();
			} else {
				_subjects.Clear ();
				LoadCachedSubjects ();
			}
		}

		public override bool OnSupportNavigateUp ()
		{
			OnBackPressed ();
			return base.OnSupportNavigateUp ();
		}

		public void LoadListContent ()
		{
			RunOnUiThread (async () => {
				_subjectsQuery = await _repo.GetSubjects ();
				foreach (var items in _subjectsQuery) {
					_subjects.Add (items.Get<string> ("subjectName"));
					_listItems.Add (items.Get<string> ("subjectName") + " : " + items.Get<int> ("grade") + "%");

					var sqliteSched = new SQLTable_Subject_Grade{
						Day = items.Get<string>("day"), 
						SubjectName = items.Get<string>("subjectName"),
						TimeIn = items.Get<string>("timeIn"), 
						TimeOut = items.Get<string>("timeOut"), 
						Instructor = items.Get<string>("instructor"),
						Location = items.Get<string>("location"),
						Grade = items.Get<int>("grade")
					};

					var query = _conn.Table<SQLTable_Subject_Grade> ().Where(p =>p.SubjectName == sqliteSched.SubjectName).Count();
					if(query < 1)
						_conn.Insert(sqliteSched);
				}

				var adapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, _listItems.ToArray ());
				_subjectList.Adapter = adapter;
			});
		}

		public void LoadCachedSubjects(){
			var query = _conn.Table<SQLTable_Subject_Grade> ();
			foreach (var items in query) {
				_subjects.Add (items.SubjectName);
				_listItems.Add (items.SubjectName + " : " + items.Grade + "%");
			}

			var adapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, _listItems.ToArray ());
			_subjectList.Adapter = adapter;
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.menus, menu);
			return base.OnPrepareOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.logout:
				{
					StartActivity (typeof(MainActivity));
					ParseUser.LogOut ();

					if (_isOnline) {
						_conn.DeleteAll<SQLTable_Schedule> ();
						_conn.DeleteAll<SQLTable_Subject> ();
						_conn.DeleteAll<SQLTable_Task> ();
						_conn.DeleteAll<SQLTable_Subject_Grade> ();
					}
					return true;
				}
				break;
			case Resource.Id.refresh:
				{
					if (_isOnline) {
						_subjects.Clear ();
						_listItems.Clear ();
						LoadListContent ();
					}
					return true;
				}
				break;
			}

			return base.OnOptionsItemSelected (item);
		}
	}
}