﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using V7ToolBar = Android.Support.V7.Widget.Toolbar;
using Parse;
using Android.Graphics;

namespace StudentHelper
{
	[Activity (Label = "Student Helper - Sign Up")]			
	public class SIgnupActivity : AppCompatActivity
	{

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			bool emailValid = false, passValid = false;

			SetContentView (Resource.Layout.Signup);
			var toolBar = FindViewById<V7ToolBar> (Resource.Id.toolBar);
			SetSupportActionBar (toolBar);

			var cancel = FindViewById<Button> (Resource.Id.cancelButton);
			cancel.Click += delegate {
				OnBackPressed ();
			};

			var fullN = FindViewById<EditText> (Resource.Id.fullName);
			var email = FindViewById<EditText> (Resource.Id.email);
			var userN = FindViewById<EditText> (Resource.Id.userName);
			var pass = FindViewById<EditText> (Resource.Id.passWord);
			var signUpBUtton = FindViewById<Button> (Resource.Id.signUp);
			var pass2 = FindViewById<EditText> (Resource.Id.passWord2);
			var warning = FindViewById<TextView> (Resource.Id.warningPass);
			var warningEmail = FindViewById<TextView> (Resource.Id.warningEmail);
			warning.Visibility = ViewStates.Gone;
			warningEmail.Visibility = ViewStates.Gone;

			pass2.TextChanged += (sender, e) => {
				var val = pass2.Text;
				if(val != pass.Text){
					warning.Text = "Password doesn't match";
					warning.Visibility = ViewStates.Visible;
					warning.SetTextColor(Color.Red);
					passValid = false;
				}else{
					warning.Text = "Password matched!";
					warning.SetTextColor(Color.Green);
					passValid = true;
				}
			};

			email.TextChanged += (sender, e) => {
				var val = email.Text;
				if(!isEmailValid(email.Text)){
					warningEmail.Text = "Invalid email pattern!";
					warningEmail.Visibility = ViewStates.Visible;
					warningEmail.SetTextColor(Color.Red);
					emailValid = false;
				}else{
					warningEmail.Text = "Nice! Good job.";
					warningEmail.SetTextColor(Color.Green);
					emailValid = true;
				}
			};

			signUpBUtton.Click += delegate {
				if(emailValid && passValid && email.Text != "" && userN.Text != "" && pass.Text != "" && fullN.Text != ""){
					if(userN.Text.Length < 8 || pass.Text.Length < 8){
						Toast.MakeText(this, "Your username or password must not go less than 8 characters", ToastLength.Short).Show();
						return;
					}
					SignUp (fullN.Text, email.Text, userN.Text, pass.Text);
				}
				else
					Toast.MakeText(this, "Form is not yet finalized. Please double check.", ToastLength.Short).Show();
			};
		}

		async void SignUp (string fullName, string email, string userName, string password)
		{
			try {
				var user = new ParseUser () {
					Username = userName,
					Password = password,
					Email = email
				};
				user ["FullName"] = fullName;

				var query = await ParseUser.Query.WhereEqualTo("username", user.Username).CountAsync();
				Console.WriteLine (query);
				if(query > 0){
					Toast.MakeText(this, userName + " is already taken.", ToastLength.Short).Show();
				}else{
					await user.SignUpAsync ();
					ParseUser.LogOut();
					Toast.MakeText (this, "Registration successful. Proceed to Login now.", ToastLength.Long).Show ();
					OnBackPressed (); 
				}

			} catch (Exception ex) {
				string message = "Oops! Something went wrong. Please try again. \n Try logging in to the previous account, then log out.";
				Toast.MakeText (this, message, ToastLength.Short).Show ();
				Console.WriteLine (ex.Message);
			}
		}

		bool isEmailValid(string email){
			return Android.Util.Patterns.EmailAddress.Matcher (email).Matches ();
		}
	}
}

