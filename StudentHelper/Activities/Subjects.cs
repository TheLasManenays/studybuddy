﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;

using V7ToolBar = Android.Support.V7.Widget.Toolbar;
using Parse;
using SQLite;
using Android.Net;

namespace StudentHelper
{
	[Activity (Label = "Subjects")]			
	public class Subjects : AppCompatActivity
	{
		ListView _subjectList;
		Repository _repo = new Repository ();
		IEnumerable<ParseObject> _subjectsQuery;
		List<Subject> _subjects = new List<Subject> ();

		bool _isOnline;

		string folder = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
		SQLiteConnection _conn;

		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			_conn = new SQLiteConnection (System.IO.Path.Combine (folder, "cached.db"));

			SetContentView (Resource.Layout.subjectListLayout);
			var toolBar = FindViewById<V7ToolBar> (Resource.Id.toolBar);
			SetSupportActionBar (toolBar);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);

			_subjectList = FindViewById<ListView> (Resource.Id.subjectList);

			var buttonAdd = FindViewById<Button> (Resource.Id.addsubject);
			buttonAdd.Click += delegate {
				if (_isOnline) {
					StartActivity(typeof(SubjectFormActivity));
				}
				else
					Toast.MakeText(this, "You are offline. Changes here won't take effect.", ToastLength.Short).Show();

//				LayoutInflater li = LayoutInflater.From (this);
//				View ap = li.Inflate (Resource.Layout.SubjectForm, null);
//				var alert = new Android.Support.V7.App.AlertDialog.Builder (this);
//				alert.SetView (ap);
//				alert.SetTitle ("New Subject");
//				var subjectName = ap.FindViewById<EditText> (Resource.Id.subjectName);
//				var hourPicker = ap.FindViewById<TimePicker> (Resource.Id.subjectTimePicker);
//				var hourPicker2 = ap.FindViewById<TimePicker> (Resource.Id.subjectTimePicker2);
//				var day = ap.FindViewById<Spinner> (Resource.Id.daySpinner);
//				var location = ap.FindViewById<EditText>(Resource.Id.locationText);
//				var instructor = ap.FindViewById<EditText>(Resource.Id.instructorText);
//
//				string dayString = string.Empty;
//				day.ItemSelected += (sender, e) => {
//					dayString = day.GetItemAtPosition (e.Position).ToString ();
//				};
//
//				var adapter = ArrayAdapter.CreateFromResource (this, Resource.Array.day_array, Android.Resource.Layout.SimpleSpinnerItem);
//				adapter.SetDropDownViewResource (Android.Resource.Layout.SimpleSpinnerDropDownItem);
//				day.Adapter = adapter;
//
//				alert.SetNegativeButton ("Cancel", delegate {
//
//				});
//				alert.SetPositiveButton ("Add", delegate {
//					string am_pm = ((hourPicker.CurrentHour).IntValue () < 12) ? "AM" : "PM";
//					var hour = hourPicker.CurrentHour + ":" + hourPicker.CurrentMinute + " " + am_pm;
//					string am_pm2 = ((hourPicker2.CurrentHour).IntValue () < 12) ? "AM" : "PM";
//					var hour2 = hourPicker2.CurrentHour + ":" + hourPicker2.CurrentMinute + " " + am_pm2;
//					_repo.AddSubject (subjectName.Text, hour, hour2, dayString, instructor.Text, location.Text);
//					Toast.MakeText (this, " Successfully added a subject.", ToastLength.Short).Show ();
//					_subjects.Clear ();
//					LoadListContent ();
//				});
//				alert.Show ();
			};

			_subjectList.ItemClick += (sender, e) => {
				Intent next = new Intent (this, typeof(Subject_Master));
				try {
					next.PutExtra ("Subject", _subjects [e.Position].SubjectName);
					next.PutExtra ("Day", _subjects [e.Position].Day);
					next.PutExtra ("Hour", _subjects [e.Position].TimeIn);
					next.PutExtra ("Hour2", _subjects [e.Position].TimeOut);
					next.PutExtra ("Instructor", _subjects [e.Position].Instructor);
					next.PutExtra ("Location", _subjects [e.Position].Location);
					StartActivity (next);
				} catch (Exception ex) {
					Console.WriteLine (ex.Message);
				}
			};

			_subjectList.ItemLongClick += (sender, e) => {
				var alert = new Android.Support.V7.App.AlertDialog.Builder (this);
				alert.SetTitle ("Options");
				alert.SetMessage ("Do you want to delete this subject?");
				alert.SetNegativeButton ("No", delegate {

				});
				alert.SetPositiveButton ("Yes", async delegate {
					_subjectsQuery = await _repo.DeleteSubject (_subjects [e.Position].SubjectName);
					_subjects.Clear ();
					LoadListContent ();
				});
				alert.Show ();
			};
		}

		protected override void OnResume ()
		{
			base.OnResume ();

			NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
			_isOnline = (activeConnection != null) && activeConnection.IsConnected;

			if (_isOnline) {
				_subjects.Clear ();
				LoadListContent ();
			} else {
				_subjects.Clear ();
				LoadCachedSubjects ();
			}
		}

		public override bool OnSupportNavigateUp ()
		{
			OnBackPressed ();
			return base.OnSupportNavigateUp ();
		}

		public void LoadListContent ()
		{
			var subjects = new List<string> ();
			RunOnUiThread (async () => {
				_subjectsQuery = await _repo.GetSubjects ();
				foreach (var items in _subjectsQuery) {
					var newSubject = new Subject () {
						SubjectName = items.Get<string> ("subjectName"),
						Day = items.Get<string> ("day"),
						TimeIn = items.Get<string>("timeIn"), 
						TimeOut = items.Get<string>("timeOut"), 
						Instructor = items.Get<string>("instructor"), 
						Location = items.Get<string>("location")
					};

					var sqliteSched = new SQLTable_Subject{
						Day = items.Get<string>("day"), 
						SubjectName = items.Get<string>("subjectName"),
						TimeIn = items.Get<string>("timeIn"), 
						TimeOut = items.Get<string>("timeOut"), 
						Instructor = items.Get<string>("instructor"),
						Location = items.Get<string>("location")
					};

					var query = _conn.Table<SQLTable_Subject> ().Where(p =>p.SubjectName == sqliteSched.SubjectName).Count();
					if(query < 1)
						_conn.Insert(sqliteSched);

					subjects.Add(items.Get<string> ("subjectName"));
					_subjects.Add (newSubject);
				}

				var adapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, subjects.ToArray ());
				_subjectList.Adapter = adapter;
			});
		}

		void LoadCachedSubjects(){
			var subjects = new List<string> ();
			var query = _conn.Table<SQLTable_Subject> ();
			foreach (var items in query) {
				var newSubject = new Subject () {
					SubjectName = items.SubjectName,
					Day = items.Day,
					TimeIn = items.TimeIn, 
					TimeOut = items.TimeOut, 
					Instructor = items.Instructor, 
					Location = items.Location
				};

				subjects.Add(items.SubjectName);
				_subjects.Add (newSubject);
			}

			var adapter = new ArrayAdapter<string> (this, Android.Resource.Layout.SimpleListItem1, subjects.ToArray ());
			_subjectList.Adapter = adapter;
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.menus, menu);
			return base.OnPrepareOptionsMenu (menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.logout:
				{
					StartActivity (typeof(MainActivity));
					ParseUser.LogOut ();
					if (_isOnline) {
						_conn.DeleteAll<SQLTable_Schedule> ();
						_conn.DeleteAll<SQLTable_Subject> ();
						_conn.DeleteAll<SQLTable_Task> ();
						_conn.DeleteAll<SQLTable_Subject_Grade> ();
					}
					return true;
				}
				break;
			case Resource.Id.refresh:
				{
					if (_isOnline) {
						_subjects.Clear ();
						LoadListContent ();
					}
					return true;
				}
				break;
			}

			return base.OnOptionsItemSelected (item);
		}
	}
}

