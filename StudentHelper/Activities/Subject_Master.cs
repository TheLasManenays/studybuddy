﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Support.V4.View;
using Android.Support.Design.Widget;
using Fragment = Android.Support.V4.App.Fragment;

using FragmentManager = Android.Support.V4.App.FragmentManager;

namespace StudentHelper
{
	[Activity (Label = "Subject Dashboard")]			
	public class Subject_Master : AppCompatActivity
	{
		string _subject, _hour, _hour2, _day, _instructor, _location;
		protected override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			SetContentView (Resource.Layout.subject_master);
			Toolbar toolBar = FindViewById<Toolbar> (Resource.Id.toolbar);
			SetSupportActionBar (toolBar);
			SupportActionBar.SetDisplayHomeAsUpEnabled (true);

			_subject = this.Intent.GetStringExtra ("Subject");
			_day = this.Intent.GetStringExtra ("Day");
			_hour = this.Intent.GetStringExtra ("Hour");
			_hour2 = this.Intent.GetStringExtra ("Hour2");
			_instructor = this.Intent.GetStringExtra ("Instructor");
			_location = this.Intent.GetStringExtra ("Location");

			GlobalContext.CurrentSubject = _subject;

			var viewPager = FindViewById<ViewPager> (Resource.Id.viewpager);
			if (viewPager != null) {
				setupViewPager (viewPager);
			}

			var tabLayout = FindViewById<TabLayout> (Resource.Id.tabs);
			tabLayout.SetupWithViewPager (viewPager);

			var surfaceOrientation = WindowManager.DefaultDisplay.Rotation;
			if (surfaceOrientation == SurfaceOrientation.Rotation0 || surfaceOrientation == SurfaceOrientation.Rotation180) {
				tabLayout.TabMode = TabLayout.ModeScrollable;

			} else {
				tabLayout.TabMode = TabLayout.ModeFixed;
			}

			this.Window.SetSoftInputMode (SoftInput.AdjustPan);
		}

		public override bool OnSupportNavigateUp ()
		{
			OnBackPressed ();
			return base.OnSupportNavigateUp ();
		}

		void setupViewPager (ViewPager viewPager)
		{
			var adapter = new Adapter (SupportFragmentManager);
			adapter.AddFragment (new subject_details_fragment ().getInstance (this, _subject, _day, (_hour + " - " + _hour2), _instructor, _location), "Subject Details");
			adapter.AddFragment (new subject_grades_fragment ().getInstance (this, _subject), "Grades");
			adapter.AddFragment (new subject_files_fragment ().getInstance (this, _subject), "Files");
			viewPager.Adapter = adapter;
		}

		class Adapter : Android.Support.V4.App.FragmentPagerAdapter
		{
			List<Fragment> fragments = new List<Fragment> ();
			List<string> fragmentTitles = new List<string> ();

			public Adapter (FragmentManager fm) : base (fm)
			{
			}

			public void AddFragment (Fragment fragment, String title)
			{
				fragments.Add (fragment);
				fragmentTitles.Add (title);
			}

			public override Fragment GetItem (int position)
			{
				return fragments [position];
			}

			public override int Count {
				get { return fragments.Count; }
			}

			public override Java.Lang.ICharSequence GetPageTitleFormatted (int position)
			{
				return new Java.Lang.String (fragmentTitles [position]);
			}

		}
	}
}

