﻿using System;
using SQLite;

namespace StudentHelper
{
	public class SQLTable_Subject_Grade
	{
		[PrimaryKey, AutoIncrement]
		public int Subject_ID {
			get;
			set;
		}

		public string SubjectName {
			get;
			set;
		}

		public string Day {
			get;
			set;
		}

		public string TimeIn {
			get;
			set;
		}

		public string TimeOut {
			get;
			set;
		}

		public string Instructor {
			get;
			set;
		}

		public string Location {
			get;
			set;
		}

		public int Grade {
			get;
			set;
		}
	}
}

