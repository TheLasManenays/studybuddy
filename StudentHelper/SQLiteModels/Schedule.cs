﻿using System;
using SQLite;

namespace StudentHelper
{
	public class SQLTable_Schedule
	{
		[PrimaryKey, AutoIncrement]
		public int Sched_ID {
			get;
			set;
		}

		public string Day {
			get;
			set;
		}

		public string Subject {
			get;
			set;
		}

		public string Instructor {
			get;
			set;
		}

		public string Location {
			get;
			set;
		}

		public string TimeIn {
			get;
			set;
		}

		public string TimeOut {
			get;
			set;
		}
	}
}

