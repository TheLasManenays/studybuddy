﻿using System;
using SQLite;

namespace StudentHelper
{
	public class SQLTable_Task
	{
		[PrimaryKey, AutoIncrement]
		public int Task_ID {
			get;
			set;
		}
		public string TaskName {
			get;
			set;
		}

		public string TaskDescription {
			get;
			set;
		}

		public string Priority {
			get;
			set;
		}

		public string Subject {
			get;
			set;
		}

		public string Deadline {
			get;
			set;
		}

		public bool isDone {
			get;
			set;
		}
	}
}

