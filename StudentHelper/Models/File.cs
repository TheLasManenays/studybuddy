﻿using System;

namespace StudentHelper
{
	public class FileObject
	{
		public string FileName {
			get;
			set;
		}

		public string Subject {
			get;
			set;
		}

		public string Link {
			get;
			set;
		}
	}
}

