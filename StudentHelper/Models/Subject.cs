﻿using System;

namespace StudentHelper
{
	public class Subject
	{
		public string SubjectName {
			get;
			set;
		}

		public string Day {
			get;
			set;
		}

		public string TimeIn {
			get;
			set;
		}

		public string TimeOut {
			get;
			set;
		}

		public string Instructor {
			get;
			set;
		}

		public string Location {
			get;
			set;
		}
	}
}

