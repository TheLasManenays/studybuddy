﻿using System;

namespace StudentHelper
{
	public class Task
	{
		public string TaskName {
			get;
			set;
		}

		public string TaskDescription {
			get;
			set;
		}

		public string Priority {
			get;
			set;
		}

		public string Subject {
			get;
			set;
		}

		public string Deadline {
			get;
			set;
		}

		public bool isDone {
			get;
			set;
		}
	}
}

