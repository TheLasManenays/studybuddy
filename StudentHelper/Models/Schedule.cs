﻿using System;

namespace StudentHelper
{
	public class Schedule
	{
		public string Day {
			get;
			set;
		}

		public string Subject {
			get;
			set;
		}

		public string Instructor {
			get;
			set;
		}

		public string Location {
			get;
			set;
		}

		public string TimeIn {
			get;
			set;
		}

		public string TimeOut {
			get;
			set;
		}
	}
}

