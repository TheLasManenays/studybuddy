﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Parse;
using Android.Support.V7.Widget;
using Android.Net;
using SQLite;

namespace StudentHelper
{
	public class subject_grades_fragment : Android.Support.V4.App.Fragment
	{
		Repository _repo = new Repository ();
		Activity _context;
		View rootView;
		ListView _criteriaList;
		TextView _gradeView;
		string _subject;
		List<string> _criterias = new List<string> ();
		List<string> _listItems = new List<string> ();
		IEnumerable<ParseObject> _criteriasQuery;
		string folder = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
		SQLiteConnection _conn;
		public subject_grades_fragment getInstance (Activity context, string subject)
		{
			subject_grades_fragment fragment = new subject_grades_fragment ();
			fragment._context = context;
			fragment._subject = subject;
			return fragment;
		}
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			HasOptionsMenu = true;
			RetainInstance = true;
			_conn = new SQLiteConnection (System.IO.Path.Combine (folder, "cached.db"));
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			rootView = inflater.Inflate (Resource.Layout.SubjectOverview, container, false);
			var toolBar = rootView.FindViewById<Toolbar> (Resource.Id.toolBar);
			toolBar.Visibility = ViewStates.Gone;

			Button addCriteria = rootView.FindViewById<Button> (Resource.Id.addCriteria);
			addCriteria.Click += (sender, e) => {
				Console.WriteLine ("Add Criteria");
				LayoutInflater li = LayoutInflater.From (_context);
				View ap = li.Inflate (Resource.Layout.NewCriteriaForm, null);
				var alert = new Android.Support.V7.App.AlertDialog.Builder (_context);
				alert.SetView (ap);
				alert.SetTitle("New Criteria");
				var criteriaName = ap.FindViewById<EditText> (Resource.Id.criteriaName);
				var percentageVal = ap.FindViewById<EditText> (Resource.Id.percentageVal);
				alert.SetNegativeButton ("Cancel", delegate {

				});
				alert.SetPositiveButton ("Add", delegate {
					_repo.AddCriteria (_subject, criteriaName.Text, Int32.Parse (percentageVal.Text));
					Toast.MakeText (_context, " Successfully added a criteria.", ToastLength.Short).Show ();
					_criterias.Clear ();
					_listItems.Clear ();
					LoadListContent ();
				});
				alert.Show ();
			};

			_gradeView = rootView.FindViewById<TextView> (Resource.Id.gradeTotalView);
			_criteriaList = rootView.FindViewById<ListView> (Resource.Id.criteriaListView);
			_criteriaList.ItemClick += (sender, e) => {
				LayoutInflater li = LayoutInflater.From (_context);
				View ap = li.Inflate (Resource.Layout.UpdateScore, null);
				var alert = new Android.Support.V7.App.AlertDialog.Builder (_context);
				alert.SetView (ap);
				var critScore = ap.FindViewById<EditText> (Resource.Id.critScore);
				var critOverAll = ap.FindViewById<EditText> (Resource.Id.critOverall);

				alert.SetPositiveButton ("Update", async delegate {
					_criteriasQuery = await _repo.UpdateCriteria (_subject, _criterias [e.Position], Int32.Parse (critScore.Text), Int32.Parse (critOverAll.Text));
					_criterias.Clear ();
					_listItems.Clear ();
					LoadListContent ();
					CalculateGrade ();
					Toast.MakeText (_context, "Update Done.", ToastLength.Short).Show ();
				});

				alert.SetNegativeButton ("Cancel", delegate {

				});

				alert.Show ();
			};
			_criteriaList.ItemLongClick += (sender, e) => {
				var alert = new Android.Support.V7.App.AlertDialog.Builder (_context);
				alert.SetTitle("Options");
				alert.SetMessage("Do you want to delete this criteria?");
				alert.SetNegativeButton("No", delegate {

				});
				alert.SetPositiveButton("Yes",async delegate {
					_criteriasQuery = await _repo.DeleteCriteria(_criterias[e.Position], _subject);
					_criterias.Clear ();
					_listItems.Clear ();
					CalculateGrade ();
					LoadListContent ();
					Toast.MakeText (_context, "Criteria deleted.", ToastLength.Short).Show ();
				});
				alert.Show();
			};
			return rootView;
		}

		public override void OnStart ()
		{
			base.OnStart ();
			NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
			bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

			if (isOnline) {
				LoadListContent ();
			}
		}

		public void LoadListContent ()
		{
			_context.RunOnUiThread (async () => {
				_criteriasQuery = await _repo.GetCriterias (_subject);
				foreach (var items in _criteriasQuery) {
					try {
						_criterias.Add (items.Get<string> ("criteriaName"));
//						Console.WriteLine (items.Get<string> ("criteriaName") + " : " + items.Get<int> ("percentage") + "%\t Recent score : " + items.Get<int> ("totalScore"));
						_listItems.Add (items.Get<string> ("criteriaName") + " : " + items.Get<int> ("percentage") + "%\nRecent score : " + items.Get<int> ("totalScore") + "/" + items.Get<int>("overAllScore"));
					} catch (Exception e) {
						Console.WriteLine (e.Message);
					}

					var masterQuery = ParseObject.GetQuery ("Subject").WhereEqualTo ("createdBy", ParseUser.CurrentUser).WhereEqualTo("subjectName", _subject);
					var gradeQuery =  await masterQuery.FirstOrDefaultAsync ();
					_gradeView.Text = "Grade Total : " + (gradeQuery.Get<int>("grade")).ToString();
				}
				var adapter = new ArrayAdapter<string> (_context, Android.Resource.Layout.SimpleListItem1, _listItems.ToArray ());
				_criteriaList.Adapter = adapter;
			});
		}

		public void CalculateGrade ()
		{
			try {
				int grade = 0;
				foreach (var item in _criteriasQuery) {
					Console.WriteLine (item.Get<string> ("criteriaName") + " " + item.Get<int> ("percentage"));
					double totalScore = item.Get<int> ("totalScore");
					double overAll = item.Get<int> ("overAllScore");
					double percentage = item.Get<int> ("percentage");
					double points = (totalScore / overAll) * percentage;

					grade += Convert.ToInt32 (points);
				}
				Console.WriteLine (grade);
				_repo.UpdateGrade(_subject,grade);
			} catch (Exception e) {
				Toast.MakeText (_context, e.Message, ToastLength.Long).Show ();
			}
		}

		public override void OnCreateOptionsMenu (IMenu menu, MenuInflater inflater)
		{
			base.OnCreateOptionsMenu (menu, inflater);
			inflater.Inflate (Resource.Menu.menus, menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.logout:
				{
					_context.StartActivity (typeof(MainActivity));
					ParseUser.LogOut ();
					NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
					bool isOnline = (activeConnection != null) && activeConnection.IsConnected;
					if (isOnline) {
						_conn.DeleteAll<SQLTable_Schedule> ();
						_conn.DeleteAll<SQLTable_Subject> ();
						_conn.DeleteAll<SQLTable_Task> ();
						_conn.DeleteAll<SQLTable_Subject_Grade> ();
					}
					return true;
				}
				break;
			case Resource.Id.refresh:
				{
					NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
					bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

					if (isOnline) {
						_criterias.Clear ();
						_listItems.Clear ();
						LoadListContent ();
					}
					return true;
				}
				break;
			}

			return base.OnOptionsItemSelected (item);
		}
	}
}

