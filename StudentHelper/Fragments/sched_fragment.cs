﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Parse;
using SQLite;
using Android.Net;

namespace StudentHelper
{
	[Activity (Label = "sched_fragment")]			
	public class sched_fragment : Android.Support.V4.App.Fragment
	{
		Activity _context;
		string _day;
		View rootView;
		ListView _scheduleList;
		List<Schedule> _schedList = new List<Schedule>();
		Repository _repo = new Repository();

		string folder = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
		SQLiteConnection _conn;
		public sched_fragment getInstance (Activity context, string day)
		{
			sched_fragment fragment = new sched_fragment ();
			fragment._context = context;
			fragment._day = day;
			return fragment;
		}

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			_conn = new SQLiteConnection (System.IO.Path.Combine (folder, "cached.db"));
			HasOptionsMenu = true;
			RetainInstance = true;
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			rootView = inflater.Inflate (Resource.Layout.sched_fragment, container, false);

			_scheduleList = rootView.FindViewById<ListView> (Resource.Id.schedList);

			return rootView;
		}

		public override void OnStart ()
		{
			base.OnStart ();
			NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
			bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

			if (isOnline) {
				LoadClassSchedules ();
			} else {
				if (_day == "All")
					LoadCachedSchedules ();
				else
					LoadCachedSchedules (_day);
			}
		}

		public void LoadClassSchedules(){
			_context.RunOnUiThread (async () => {
				_schedList.Clear();
				IEnumerable<ParseObject> subjectsQuery;
				if(_day == "All")
					subjectsQuery = await _repo.GetSchedules ();
				else
					subjectsQuery = await _repo.GetSchedules (_day);
				
				foreach (var items in subjectsQuery) {
					var sched = new Schedule(){
						Day = items.Get<string>("day"), 
						Subject = items.Get<string>("subjectName"),
						TimeIn = items.Get<string>("timeIn"), 
						TimeOut = items.Get<string>("timeOut"), 
						Instructor = items.Get<string>("instructor"),
						Location = items.Get<string>("location")
					};

					var sqliteSched = new SQLTable_Schedule{
						Day = items.Get<string>("day"), 
						Subject = items.Get<string>("subjectName"),
						TimeIn = items.Get<string>("timeIn"), 
						TimeOut = items.Get<string>("timeOut"), 
						Instructor = items.Get<string>("instructor"),
						Location = items.Get<string>("location")
					};

					var query = _conn.Table<SQLTable_Schedule> ().Where(p =>p.Subject == sqliteSched.Subject).Count();
					if(query < 1)
						_conn.Insert(sqliteSched);
					_schedList.Add(sched);
				}
				SchedListAdapter adapter = new SchedListAdapter(_context,_schedList);
				_scheduleList.Adapter = adapter;
			});
		}

		void LoadCachedSchedules(string day){
			_schedList.Clear();
			var query = _conn.Table<SQLTable_Schedule> ().Where(p=>p.Day == day);
			foreach (var items in query) {
				var sched = new Schedule(){
					Day = items.Day, 
					Subject = items.Subject,
					TimeIn = items.TimeIn, 
					TimeOut = items.TimeOut, 
					Instructor = items.Instructor,
					Location = items.Location
				};
				_schedList.Add(sched);
			}
			SchedListAdapter adapter = new SchedListAdapter(_context,_schedList);
			_scheduleList.Adapter = adapter;
		}
		void LoadCachedSchedules(){
			_schedList.Clear();
			var query = _conn.Table<SQLTable_Schedule> ();
			foreach (var items in query) {
				var sched = new Schedule(){
					Day = items.Day, 
					Subject = items.Subject,
					TimeIn = items.TimeIn, 
					TimeOut = items.TimeOut, 
					Instructor = items.Instructor,
					Location = items.Location
				};
				_schedList.Add(sched);
			}
			SchedListAdapter adapter = new SchedListAdapter(_context,_schedList);
			_scheduleList.Adapter = adapter;
		}

		public override void OnCreateOptionsMenu (IMenu menu, MenuInflater inflater)
		{
			base.OnCreateOptionsMenu (menu, inflater);
			inflater.Inflate (Resource.Menu.mainmenu, menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.logout:
				{
					_context.StartActivity (typeof(MainActivity));
					ParseUser.LogOut ();
					NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
					bool isOnline = (activeConnection != null) && activeConnection.IsConnected;
					if (isOnline) {
						_conn.DeleteAll<SQLTable_Schedule> ();
						_conn.DeleteAll<SQLTable_Subject> ();
						_conn.DeleteAll<SQLTable_Task> ();
						_conn.DeleteAll<SQLTable_Subject_Grade> ();
					}
					return true;
				}
				break;
			case Resource.Id.refresh:
				{
					NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
					bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

					if (isOnline) {
						if (_day == "All")
							LoadCachedSchedules ();
						else
							LoadCachedSchedules (_day);
					}
					return true;
				}
				break;
			case Resource.Id.tasks:
				{
					_context.StartActivity (typeof(TasksActivity));
					return true;
				}
				break;
			case Resource.Id.subjects:
				{
					_context.StartActivity (typeof(Subjects));
					return true;
				}
				break;
			case Resource.Id.grades:
				{
					_context.StartActivity (typeof(DashboardActivity));
					return true;
				}
				break;
			}
			return base.OnOptionsItemSelected (item);
		}
	}
}

