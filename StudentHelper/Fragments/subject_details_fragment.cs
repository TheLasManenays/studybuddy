﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using V7ToolBar = Android.Support.V7.Widget.Toolbar;
using Parse;
using SQLite;
using Android.Net;

namespace StudentHelper
{
	[Activity (Label = "Subject Details")]			
	public class subject_details_fragment : Android.Support.V4.App.Fragment
	{
		string _subject, _day, _hour, _instructor, _location;
		Activity _context;
		View rootView;

		Repository _repo = new Repository();
		ListView _taskListView;
		IEnumerable<ParseObject> _tasks;
//		ArrayAdapter adapter;
//		List<string> _taskList = new List<string>();
		List<Task> _taskCol = new List<Task> ();
		string folder = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal);
		SQLiteConnection _conn;
		public subject_details_fragment getInstance (Activity context, string subject, string day, string hour, string instructor, string location)
		{
			subject_details_fragment fragment = new subject_details_fragment ();
			fragment._context = context;
			fragment._subject = subject;
			fragment._day = day;
			fragment._hour = hour;
			fragment._instructor = instructor;
			fragment._location = location;
			return fragment;
		}
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			HasOptionsMenu = true;
			RetainInstance = true;
			_conn = new SQLiteConnection (System.IO.Path.Combine (folder, "cached.db"));
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			rootView = inflater.Inflate (Resource.Layout.subjectDetailsLayout, container, false);
			var subjectName = rootView.FindViewById<TextView> (Resource.Id.subjectName);
			var day = rootView.FindViewById<TextView> (Resource.Id.day);
			var hour = rootView.FindViewById<TextView> (Resource.Id.hour);
			var instructor = rootView.FindViewById<TextView> (Resource.Id.instructorText);
			var location = rootView.FindViewById<TextView> (Resource.Id.locationText);
			_taskListView = rootView.FindViewById<ListView> (Resource.Id.taskList);

			subjectName.Text = _subject;
			day.Text = _day;
			hour.Text = _hour;
			instructor.Text = "Instructor: " + _instructor;
			location.Text = "Location " + _location;

			var addTask = rootView.FindViewById<Button> (Resource.Id.addTask);
			addTask.Click += delegate {
				Intent intent = new Intent(_context, typeof(TaskFormActivity));
				intent.PutExtra("FromWhere", "detailsFragment");
				intent.PutExtra("Subject", _subject);
				StartActivity(intent);
//				LayoutInflater li = LayoutInflater.From (_context);
//				View ap = li.Inflate (Resource.Layout.TaskForm, null);
//				var alert = new Android.Support.V7.App.AlertDialog.Builder (_context);
//				alert.SetView (ap);
//				alert.SetTitle("New Task");
//				var taskName = ap.FindViewById<EditText>(Resource.Id.taskName);
//				var taskDescription = ap.FindViewById<EditText>(Resource.Id.taskDescription);
//				var taskDatePicker = ap.FindViewById<DatePicker>(Resource.Id.taskDatePicker);
//				var taskTimePicker = ap.FindViewById<TimePicker>(Resource.Id.taskTimePicker);
//				var taskSubject = ap.FindViewById<EditText>(Resource.Id.taskSubject);
//
//				var priority = ap.FindViewById<Button>(Resource.Id.prority);
//				string[] priorityTypes = new string[]{"Low", "Medium", "High"};
//				int index = 0;
//				priority.Text = priorityTypes[index];
//				priority.Click += delegate {
//					if(index < 2)
//						index++;
//					else
//						index = 0;
//					priority.Text = priorityTypes[index];
//				};
//				alert.SetNegativeButton ("Cancel", delegate {
//
//				});
//				alert.SetPositiveButton ("Add", delegate {
//					string am_pm = ((taskTimePicker.CurrentHour).IntValue() < 12) ? "AM" : "PM";
//					string deadline = ("Deadline: " + (taskDatePicker.Month+1)+"/"+taskDatePicker.DayOfMonth+"/"+taskDatePicker.Year + " " + taskTimePicker.CurrentHour + ":" + taskTimePicker.CurrentMinute + " " +am_pm).ToString();
//					_repo.AddTask(taskName.Text, taskDescription.Text, priority.Text, deadline, taskSubject.Text);
//					LoadTasks();
//					Toast.MakeText (_context, " Successfully added a task.", ToastLength.Short).Show ();
//				});
//				alert.Show ();
			};

			_taskListView.ItemClick += (sender, e) => {
				var alert = new Android.Support.V7.App.AlertDialog.Builder (_context);
				alert.SetTitle(_taskCol[e.Position].TaskName);
				alert.SetMessage( "Priority: " + _taskCol[e.Position].Priority + "\n\nDescription: " + _taskCol[e.Position].TaskDescription  + "\n\n" + 
					"Deadline : " + _taskCol[e.Position].Deadline);
				alert.Show();
			};

			_taskListView.ItemLongClick += (sender, e) => {
				var alert = new Android.Support.V7.App.AlertDialog.Builder (_context);
				alert.SetTitle("Options");
				alert.SetItems(new String[]{"Delete", "Modify"},delegate(object sender2, DialogClickEventArgs e2) {
					switch (e2.Which){
					case 0 :{
							var alert3 = new Android.Support.V7.App.AlertDialog.Builder (_context);
							alert3.SetTitle(_taskCol[e.Position].TaskName);
							alert3.SetMessage("You really want to delete this task?");
							alert3.SetPositiveButton("Yes", async delegate {
								_tasks = await _repo.DeleteTask(_taskCol[e.Position].TaskName);
								LoadTasks();
								Toast.MakeText(_context,"Delete Completely.",ToastLength.Short);
							});
							alert3.SetNegativeButton("No", delegate {

							});
							alert3.Show();
							//							Toast.MakeText(this, "done", ToastLength.Short).Show();
						}
						break;
					case 1 :{
							LayoutInflater li = LayoutInflater.From (_context);
							View ap = li.Inflate (Resource.Layout.TaskForm, null);
							var alert2 = new Android.Support.V7.App.AlertDialog.Builder (_context);
							alert2.SetView (ap);
							alert2.SetTitle(_taskCol[e.Position].TaskName);
							var taskName = ap.FindViewById<EditText>(Resource.Id.taskName);
							var taskLabel = ap.FindViewById<TextView>(Resource.Id.taskLabel);
							taskName.Visibility = ViewStates.Gone; taskLabel.Visibility = ViewStates.Gone;
							var taskDescription = ap.FindViewById<EditText>(Resource.Id.taskDescription);
							var taskDatePicker = ap.FindViewById<DatePicker>(Resource.Id.taskDatePicker);
							var taskTimePicker = ap.FindViewById<TimePicker>(Resource.Id.taskTimePicker);
							var taskSubject = ap.FindViewById<EditText>(Resource.Id.taskSubject);
							taskDescription.Text = _taskCol[e.Position].TaskDescription;
							taskSubject.Text = _taskCol[e.Position].Subject;
							var priority = ap.FindViewById<Button>(Resource.Id.prority);
							string[] priorityTypes = new string[]{"Low", "Medium", "High"};
							int index = 0;
							priority.Text = priorityTypes[index];
							priority.Click += delegate {
								if(index < 2)
									index++;
								else
									index = 0;
								priority.Text = priorityTypes[index];
							};
							alert2.SetNegativeButton ("Cancel", delegate {

							});
							alert2.SetPositiveButton ("Save", async delegate {
								string am_pm = ((taskTimePicker.CurrentHour).IntValue() < 12) ? "AM" : "PM";
								string deadline = ("Deadline: " + (taskDatePicker.Month+1)+"/"+taskDatePicker.DayOfMonth+"/"+taskDatePicker.Year + " " + taskTimePicker.CurrentHour + ":" + taskTimePicker.CurrentMinute + " " +am_pm).ToString();
								_tasks = await _repo.UpdateTask(_taskCol[e.Position].TaskName, taskDescription.Text, priority.Text, deadline, taskSubject.Text);
								LoadTasks();
								Toast.MakeText (_context, " Successfully updated task.", ToastLength.Short).Show ();
							});
							alert2.Show ();
						}
						break;
					}
				});
				alert.Show();
			};
			return rootView;
		}

		public override void OnStart ()
		{
			base.OnStart ();
			NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
			bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

			if (isOnline) {
				LoadTasks ();
			} else {
				LoadCachedTasks ();
			}
		}

		public void LoadTasks(){
			_context.RunOnUiThread (async delegate {
				_tasks = await _repo.GetTasks (_subject);
				_taskCol.Clear();
//				_taskList.Clear ();
				foreach (var item in _tasks) {

					var task = new Task(){
						TaskName = item.Get<string> ("taskName"),
						TaskDescription = item.Get<string> ("taskDescription"),
						Priority = item.Get<string> ("priority"), 
						Subject = item.Get<string>("Subject"),
						Deadline = item.Get<string>("deadline"),
						isDone = item.Get<bool>("Done")
					};

					var sqliteTask = new SQLTable_Task(){
						TaskName = item.Get<string> ("taskName"),
						TaskDescription = item.Get<string> ("taskDescription"),
						Priority = item.Get<string> ("priority"), 
						Subject = item.Get<string>("Subject"),
						Deadline = item.Get<string>("deadline"),
						isDone = item.Get<bool>("Done")
					};

					var query = _conn.Table<SQLTable_Task> ().Where(p =>p.TaskName == sqliteTask.TaskName).Where(d=>d.TaskDescription == sqliteTask.TaskDescription).Count();
					if(query < 1)
						_conn.Insert(sqliteTask);

					_taskCol.Add(task);
//					_taskList.Add (item.Get<string> ("taskName") + " (" + item.Get<string> ("priority") + " Priority)");
				}
//				adapter = new ArrayAdapter<string> (_context, Android.Resource.Layout.SimpleListItem1, _taskList.ToArray ());
				var adapter = new TaskListAdapter(_context, _taskCol);
				_taskListView.Adapter = adapter;	
			});
		}

		public void LoadCachedTasks(){
			_taskCol.Clear();
			var query = _conn.Table<SQLTable_Task> ().Where (p => p.Subject == _subject);
			foreach (var item in query) {
				var task = new Task(){
					TaskName = item.TaskName,
					TaskDescription = item.TaskDescription,
					Priority = item.Priority, 
					Subject = item.Subject,
					Deadline = item.Deadline,
					isDone = item.isDone
				};
				_taskCol.Add(task);
			}
			var adapter = new TaskListAdapter(_context, _taskCol);
			_taskListView.Adapter = adapter;	
		}

		public override void OnCreateOptionsMenu (IMenu menu, MenuInflater inflater)
		{
			base.OnCreateOptionsMenu (menu, inflater);
			inflater.Inflate (Resource.Menu.menus, menu);
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			switch (item.ItemId) {
			case Resource.Id.logout:
				{
					_context.StartActivity (typeof(MainActivity));
					ParseUser.LogOut ();
					NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
					bool isOnline = (activeConnection != null) && activeConnection.IsConnected;
					if (isOnline) {
						_conn.DeleteAll<SQLTable_Schedule> ();
						_conn.DeleteAll<SQLTable_Subject> ();
						_conn.DeleteAll<SQLTable_Task> ();
						_conn.DeleteAll<SQLTable_Subject_Grade> ();
					}
					return true;
				}
				break;
			case Resource.Id.refresh:
				{
					NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
					bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

					if (isOnline) {
						LoadTasks ();
					}
					return true;
				}
				break;
			}

			return base.OnOptionsItemSelected (item);
		}
	}
}

