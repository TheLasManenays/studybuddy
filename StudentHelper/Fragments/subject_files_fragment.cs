﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Android.Media;
using Java.IO;
using Android.Database;
using Parse;
using System.Threading.Tasks;
using Android.Net;

namespace StudentHelper
{
	public class subject_files_fragment : Android.Support.V4.App.Fragment
	{
		Activity _context;
		string _subject;
		View rootView;
		Repository _repo = new Repository();
		IEnumerable<ParseObject> _filesObject;
		List<FileObject> _fileList = new List<FileObject>();
		ListView _filesListView;
		public subject_files_fragment getInstance (Activity context, string subject)
		{
			subject_files_fragment fragment = new subject_files_fragment ();
			fragment._context = context;
			fragment._subject = subject;
			return fragment;
		}
		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
			HasOptionsMenu = true;
			RetainInstance = true;
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			rootView = inflater.Inflate (Resource.Layout.SubjectFilesLayout, container, false);
			var addFile = rootView.FindViewById<Button> (Resource.Id.addFileButton);
			_filesListView = rootView.FindViewById<ListView> (Resource.Id.fileListView);
			addFile.Click += delegate {
				_context.StartActivity(typeof(FileDisplayActivity));
			};

			_context.RunOnUiThread (async delegate {
				NetworkInfo activeConnection = GlobalContext._connectivityManager.ActiveNetworkInfo;
				bool isOnline = (activeConnection != null) && activeConnection.IsConnected;

				if (isOnline) {
					LoadFiles ();
				}
			});

			_filesListView.ItemLongClick += (sender, e) => {
				var url = Android.Net.Uri.Parse(_fileList[e.Position].Link);
				var intent = new Intent(Intent.ActionView, url);
				StartActivity(intent);
			};

			return rootView;
		}

		async Task<bool> LoadFiles(){
			_context.RunOnUiThread (async delegate {
				_fileList.Clear();
				_filesObject = await _repo.GetFiles(_subject);
				foreach(var items in _filesObject){
					var file = new FileObject(){
						FileName = items.Get<string>("fileName"), 
						Link = items.Get<ParseFile>("fileDoc").Url.ToString(),
						Subject = items.Get<string>("subjectName")
					};

					_fileList.Add(file);
				}

				List<string> listItems = new List<string>();
				foreach(var files in _fileList){
					listItems.Add ("\n" + files.FileName +"\n");
				}
				var adapter = new ArrayAdapter<string> (_context, Android.Resource.Layout.SimpleListItem1, listItems.ToArray ());
				_filesListView.Adapter = adapter;
			});
			return true;
		}
	}
}

